import bgImage from "@assets/bg-image.svg";
import WithContentLayout from "@components/HOC/WithContentLayout";
import Tabs, { ITabList } from "@components/Tabs/Tabs";
import "./Contribute.scss";

const tabList: Array<ITabList> = [
  {
    label: "Overview",
    contentHTML: (
      <p>
        Lorem ipsum 1st dolor sit amet, consectetur adipiscing elit. Suspendisse
        varius enim in eros elementum tristique. Duis cursus, mi quis viverra
        ornare, eros dolor interdum nulla, ut commodo diam libero vitae erat.
        <br />
        <br />
        <br />
        Aenean faucibus nibh et justo cursus id rutrum lorem imperdiet. Nunc ut
        sem vitae risus tristique posuere.
      </p>
    ),
  },
  {
    label: "Impact",
    contentHTML: (
      <p>
        Lorem ipsum 2nd dolor sit amet, consectetur adipiscing elit. Suspendisse
        varius enim in eros elementum tristique. Duis cursus, mi quis viverra
        ornare, eros dolor interdum nulla, ut commodo diam libero vitae erat.
        <br />
        <br />
        <br />
        Aenean faucibus nibh et justo cursus id rutrum lorem imperdiet. Nunc ut
        sem vitae risus tristique posuere.
      </p>
    ),
  },
  {
    label: "What You Get",
    contentHTML: (
      <p>
        Lorem ipsum 3rd dolor sit amet, consectetur adipiscing elit. Suspendisse
        varius enim in eros elementum tristique. Duis cursus, mi quis viverra
        ornare, eros dolor interdum nulla, ut commodo diam libero vitae erat.
        <br />
        <br />
        <br />
        Aenean faucibus nibh et justo cursus id rutrum lorem imperdiet. Nunc ut
        sem vitae risus tristique posuere.
      </p>
    ),
  },
];

const Contribute = () => {
  return (
    <div className="contribute-main">
      <div className="how-to-contribute">
        <div className="left">
          <h2>How you can contribute to protect Earth</h2>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse
            varius enim in eros elementum tristique. Duis cursus, mi quis
            viverra ornare, eros dolor interdum nulla, ut commodo diam libero
            vitae erat. Aenean faucibus nibh et justo cursus id rutrum lorem
            imperdiet. Nunc ut sem vitae risus tristique posuere.
          </p>
        </div>
        <div className="right">
          <Tabs tabList={tabList} />
        </div>
      </div>
      <div className="how-use-donation">
        <h2>How we use your donation</h2>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse
          varius enim in eros elementum tristique. Duis cursus, mi quis viverra
          ornare, eros dolor interdum nulla, ut commodo diam libero vitae erat.
          Nunc ut sem vitae risus tristique posuere.
        </p>
        <p>
          Aenean faucibus nibh et justo cursus id rutrum lorem imperdiet. Nunc
          ut sem vitae risus tristique posuere. Aenean faucibus nibh et justo
          cursus id rutrum lorem imperdiet.
        </p>
      </div>
      <div className="make-greener">
        <img src={bgImage} alt="" />
        <div className="middle">
          <h2>You can contribute to make the environment greener!</h2>
          <div>
            <button>Join as a volunteer</button>
            <button>Donate</button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default WithContentLayout(Contribute);

import "./LayoutStyle.scss";

const WithContentLayout = (
  OriginalComponent: any,
  bg: string = "white",
  sectionName: string = "section"
) => {
  return (props: any) => {
    return (
      <section
        className={`${sectionName}-main content-layout`}
        style={{ backgroundColor: bg }}
      >
        <div className="container">
          <OriginalComponent {...props} />
        </div>
      </section>
    );
  };
};

export default WithContentLayout;

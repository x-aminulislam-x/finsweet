import donateImg from "../../@assets/donateImg.svg";
import WithContentLayout from "../HOC/WithContentLayout";
import "./Header.scss";

const Header = () => {
  return (
    <div className="header-main">
      <div className="header-description">
        <span className="text">DONATE</span>
        <h1>Making a donation for Nature.</h1>
        <span className="desc">
          When you donate, you’re supporting effective solutions to big
          environmental challenges—an investment in the future of our planet.
        </span>
        <button>Donate Now</button>
      </div>
      <div className="header-img">
        <img src={donateImg} alt="" />
      </div>
    </div>
  );
};

export default WithContentLayout(Header, "#EFF7F2");

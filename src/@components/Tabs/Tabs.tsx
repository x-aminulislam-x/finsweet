import { useState } from "react";
import "./Tabs.scss";
export interface ITabList {
  label: string;
  contentHTML: JSX.Element;
}

interface ITabs {
  tabList: ITabList[];
  tabActive?: number;
}

const Tabs = ({ tabList, tabActive = 0 }: ITabs) => {
  const [activeTab, setActiveTab] = useState<number>(tabActive);

  return (
    <div className="tabs-main">
      <div className="tabs-header">
        {tabList.map((header: ITabList, indx: number) => {
          return (
            <span
              key={indx}
              className={`header-item ${indx === activeTab ? "active" : ""}`}
              onClick={() => setActiveTab(indx)}
            >
              {header.label}
            </span>
          );
        })}
      </div>
      <div className="tabs-content">
        {tabList.map((tab: ITabList, indx: number) => {
          if (indx === activeTab)
            return <div key={indx}>{tab.contentHTML}</div>;
          return void 0;
        })}
      </div>
    </div>
  );
};

export default Tabs;

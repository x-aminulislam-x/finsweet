import footerImg from "@assets/White-Logo.svg";
import WithContentLayout from "@components/HOC/WithContentLayout";
import "./Footer.scss";

const footerMenuList = [
  {
    lable: "About us",
    link: "#",
  },
  {
    lable: "Projects",
    link: "#",
  },
  {
    lable: "Facebook",
    link: "#",
  },
  {
    lable: "Team",
    link: "#",
  },
  {
    lable: "Events",
    link: "#",
  },
  {
    lable: "Instagram",
    link: "#",
  },
  {
    lable: "What we do ",
    link: "#",
  },
  {
    lable: "Donate",
    link: "#",
  },
  {
    lable: "Twitter",
    link: "#",
  },
  {
    lable: "Contact",
    link: "#",
  },
  {
    lable: "Blog",
    link: "#",
  },
  {
    lable: "Linkedin",
    link: "#",
  },
];

const Footer = () => {
  return (
    <footer className="footer-main">
      <div className="footer-img">
        <img src={footerImg} alt="" />
      </div>
      <div className="footer-menu">
        <div className="footer-menu-header">
          <span>Our team</span>
          <span>More</span>
          <span>Connect</span>
        </div>
        <ul className="footer-menu-body">
          {footerMenuList.map((item, indx) => {
            return (
              <li key={indx}>
                <a href={item.link}>{item.lable}</a>
              </li>
            );
          })}
        </ul>
      </div>
      <div className="footer-form">
        <label className="email-label" htmlFor="emailInput">
          Subscribe to get latest updates{" "}
        </label>
        <div className="input-group">
          <input
            id="emailInput"
            type="email"
            name="email"
            placeholder="Your email"
          />
          <button>Subscribe</button>
        </div>
      </div>
    </footer>
  );
};

export default WithContentLayout(Footer, "black");

import logoImg from "@assets/Logo.svg";
import "./Navbar.scss";

import { useEffect } from "react";
import WithContentLayout from "../../@components/HOC/WithContentLayout";

const Navbar = () => {
  useEffect(() => {
    const ul = document.querySelector(".nav-items ul");
    const li = ul.querySelectorAll("li");

    for (let i = 0; i < li.length; i++) {
      li[i].addEventListener("click", (e: any) => {
        const current = ul.querySelectorAll(".active");

        if (current.length) {
          current[0].classList.remove("active");
        }

        e.target.classList.add("active");
      });
    }
  }, []);

  const handleSidebar = () => {
    document.getElementById("sidebar").style.width = "230px";
  };

  const handleClose = () => {
    document.getElementById("sidebar").style.width = "0";
  };

  return (
    <nav className="nav-main">
      <div className="nav-img">
        <a href="/">
          <img src={logoImg} alt="" />
        </a>
      </div>
      <div className="nav-items">
        <ul>
          <li>
            <a href="#Home" className="active">
              Home
            </a>
          </li>
          <li>
            <a href="#About-us">About Us</a>
          </li>
          <li>
            <a href="#What-we-do">What We Do</a>
          </li>
          <li>
            <a href="#Media">Media</a>
          </li>
          <li>
            <a href="#Contact">Contact</a>
          </li>
        </ul>
        <span onClick={() => handleSidebar()} className="menuIcon">
          &#9776;
        </span>
        <button>Donate</button>
      </div>
      {/* for responsive use */}
      <div id="sidebar">
        <div onClick={handleClose} className="closeIcon">
          &times;
        </div>
        <ul>
          <li>
            <a href="#Home" className="active">
              Home
            </a>
          </li>
          <li>
            <a href="#About-us">About Us</a>
          </li>
          <li>
            <a href="#What-we-do">What We Do</a>
          </li>
          <li>
            <a href="#Media">Media</a>
          </li>
          <li>
            <a href="#Contact">Contact</a>
          </li>
        </ul>
      </div>
    </nav>
  );
};

export default WithContentLayout(Navbar, "#EFF7F2");

import Contribute from "@components/Contribute/Contribute";
import Header from "./@components/Header/Header";
import Layout from "./@layouts";
import "./App.scss";

function App() {
  return (
    <div className="finsweet-app">
      <Layout>
        <Header />
        <Contribute />
      </Layout>
    </div>
  );
}

export default App;
